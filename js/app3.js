var tbody = document.querySelector('#lista');
           
function peticion(){
    
    var http = new XMLHttpRequest();
    
    http.open('GET', 'https://jsonplaceholder.typicode.com/users', true);
    
    http.onreadystatechange = function() {
        if (http.readyState == 4 && http.status == 200) {
           
           var responseData = JSON.parse(http.responseText);
            console.log(responseData);
            for(const datos of responseData){
                tbody.innerHTML += '<tr><th>'+datos.id+'</th>'+'<th>'
                +datos.name+'</th>'+'<th>'+datos.username+'</th>'+'<th>'+
                datos.email+'</th>'+'<th>'+datos.phone+'</th>'+'<th>'+datos.website+'</th></tr>'
            }
        }
    };
    
    http.send();
    }
    
    var btncargar = document.querySelector('#btncargar');
    var btnLimpiar = document.querySelector('#btnLimpiar');
    
    btncargar.addEventListener('click',function(){
        if(tbody.innerHTML==""){
        peticion()
        }else{
           alert("Se cargaron los datos")
        }
    });
    
    btnLimpiar.addEventListener('click',function(){
        document.querySelector('#lista').innerHTML="";
    });
    
    
    