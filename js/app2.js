var btnbuscar = document.querySelector('#btnbuscar');
var btnlimpiar = document.querySelector('#btnlimpiar');
var show = document.createElement('tr');
btnbuscar.addEventListener('click',()=>{
    var information = parseInt(document.querySelector('.information').value);
    
    if(isNaN(information)|information==""){
        alert('NO ES UN NUMERO')
    }else{
    var http = new XMLHttpRequest();
    http.open('GET', 'https://jsonplaceholder.typicode.com/albums/'+information, true);
    // Configurar el manejo de eventos para la respuesta
    http.onreadystatechange = function() {
    // Verificar si la solicitud se completó satisfactoriamente
        if (http.readyState == 4 && http.status == 200) {
        // La respuesta está lista y el estado es OK

        // Manejar la respuesta
            var responseData = JSON.parse(http.responseText);
            console.log(responseData);
            show.classList.add('table-row');
            show.innerHTML = '<td>'+responseData.title + '</td>';
            document.querySelector('#tabla').append(show);
        } else if (http.status == 404) {
            // El estado es 404, el recurso no fue encontrado
            alert('El número ingresado no existe');
        }
}

// Enviar la solicitud
http.send();
}
});

btnlimpiar.addEventListener('click', ()=>{
    show.innerHTML = "";
    document.querySelector('.information').value="";
})
