var btnbuscar = document.querySelector('#btnbuscar');
var btnlimpiar = document.querySelector('#btnlimpiar');
var show = document.createElement('tr');
btnbuscar.addEventListener('click',()=>{
    var information = parseInt(document.querySelector('.information').value);
    
    if(isNaN(information)){
        alert('INGRESA UN NUMERO')
    }else{
    var http = new XMLHttpRequest();
    http.open('GET', 'https://jsonplaceholder.typicode.com/users/'+information, true);
   
    http.onreadystatechange = function() {
    
        if (http.readyState == 4 && http.status == 200) {
      
            var responseData = JSON.parse(http.responseText);
            console.log(responseData);
            show.classList.add('table-row');
            show.innerHTML = '<td>'+responseData.name + '</td>';
            document.querySelector('#tabla').append(show);
        } else if (http.status == 404) {
            alert('EL NUMERO NO EXISTE');
        }
}

http.send();
}
});

btnlimpiar.addEventListener('click', ()=>{
    show.innerHTML = "";
    document.querySelector('.information').value="";
})
